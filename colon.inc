%define next_label 0

%macro colon 2
    %ifid %2
        %2:
            dq next_label
            db %1, 0

        %define next_label %2
    %else
        %error "Wrong identifier!"
    %endif
%endmacro
