%include "lib.inc"

%define QWORD 8
section .text

global find_word

;find_word пройдёт по всему словарю в поисках подходящего ключа.
;Если подходящее вхождение найдено, вернёт адрес начала вхождения в   словарь (не значения), иначе вернёт 0.

find_word:
    ;rdi - Указатель на нуль-терминированную строку.
    ;rsi - Указатель на начало словаря.

    push rsi
    mov r11, rsi

    .loop:
        add r11, QWORD
        mov rsi, r11
        call string_equals

        test rax, rax
        jnz .found

        sub r11, QWORD
        mov r11, [r11]
        test r11, r11
        jz .not_found

        jmp .loop

    .not_found:
    xor rax, rax
    jmp .return

    .found:
    mov rax, rsi

    .return:
    pop rsi
    ret
