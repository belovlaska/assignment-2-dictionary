%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define MAX_LENGTH 255
%define BUFFER_SIZE 256

section .data
    too_long_error: db "String is too long!", 0
    not_found_error: db "Word not found!", 0
    buffer: times BUFFER_SIZE db 0

section .text

global _start

_start:
    mov rdi, buffer
    mov rsi, MAX_LENGTH
    call read_word

    test rax, rax
    jz .too_long

    mov rdi, rax
    mov rsi, first_word
    call find_word

    test rax, rax
    jz .not_found

    mov rdi, rax
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi

    call print_string
    call print_newline

    call exit

    .too_long:
        mov rdi, too_long_error
        jmp .error
    .not_found:
        mov rdi, not_found_error
    .error:
        call print_error
        call print_newline
        call exit
