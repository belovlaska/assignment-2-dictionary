ASM = nasm
ASM_FLAGS = -felf64 -o
LD = ld
LD_FLAGS = -o

.PHONY: all clean test

all: program

clean:
	rm *.o
	rm program

test:
	python3 test.py

%.o: %.asm
	$(ASM) $(ASM_FLAGS) $@ $<

program: main.o dict.o lib.o
	$(LD) $(LD_FLAGS) $@ $^
