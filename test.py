import subprocess
import os

inputs = ["", "first word", "second word", "third word", "a", "1" * 257]
expected_outputs = ["", "first word explanation", "second word explanation", "third word explanation", "", ""]
expected_errors = ["Word not found!", "", "", "", "Word not found!", "String is too long!"]

errors = []

if os.path.exists("./program"):
    print("Starting test")

    for i in range(len(inputs)):
        process = subprocess.Popen(["./program"], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate(input=inputs[i].encode())

        stdout = stdout.decode().strip()
        stderr = stderr.decode().strip()

        if (stdout == expected_outputs[i] and stderr == expected_errors[i]):
            print("_", end="")
        else:
            print("F", end="")
            if stdout != expected_outputs[i]:
                errors.append("Wrong stdout " + stdout + ", expected " + expected_outputs[i])
            if stderr != expected_errors[i]:
                errors.append("Wrong stderr " + stderr + " expected " + expected_errors[i])

    print("\n")
    if len(errors) != 0:
        for string in errors:
            print(string)
    else:
        print("OK")
else:
    print("Executable file \"program\" does not exist")
